package com.zhouyi.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhouyi.entity.UserEntity;
import com.zhouyi.utils.RedisUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class RedisController {

    @Autowired
    private RedisUtil redisUtil;

    // http://localhost:8080/del?key=test
    @RequestMapping("/del")
    public boolean del(String... key) {
        boolean del = redisUtil.del(key);
        log.info("del result:{}", del);
        
        return del;
    }

    // ---------------------string类型---begin---------------------------------
    // http://localhost:8080/set?id=1&name=zhouyi&age=35&key=test
    @RequestMapping("/set")
    public boolean set(String key, UserEntity user) {
        log.info("set key:{},value:{}", key, user);
        return redisUtil.set(key, user);
    }

    // http://localhost:8080/get?key=test
    @RequestMapping("/get")
    public Object get(String key) {
        Object object = redisUtil.get(key);
        log.info("get key:{},value:{}", key, object);
        return object;
    }
    // ---------------------string类型---end---------------------------------

    // ---------------------list类型---begin---------------------------------

    // http://localhost:8080/lSet?id=1&name=zhouyi&age=35&key=test
    @RequestMapping("/lSet")
    public boolean lSet(String key, UserEntity user) {
        log.info("lSet key:{},value:{}", key, user);
        return redisUtil.lSet(key, user);
    }

    // http://localhost:8080/lGetList?key=test&start=0&end=2
    @RequestMapping("/lGetList")
    public List<Object> lGetList(String key, long start, long end) {
        List<Object> objectList = redisUtil.lGet(key, start, end);
        log.info("lGetList key:{},value:{}", key, objectList);
        return objectList;
    }

    // http://localhost:8080/lGet?key=test&index=0
    @RequestMapping("/lGet")
    public Object lGet(String key, long index) {
        Object object = redisUtil.lGetIndex(key, index);
        log.info("lGet key:{},value:{}", key, object);
        return object;
    }

    // http://localhost:8080/lGetListSize?key=test
    @RequestMapping("/lGetListSize")
    public long lGetListSize(String key) {
        long lGetListSize = redisUtil.lGetListSize(key);
        log.info("lGetListSize key:{},size:{}", key, lGetListSize);
        return lGetListSize;
    }

    // ---------------------list类型---end---------------------------------
    // ---------------------Set类型---begin---------------------------------
    // http://localhost:8080/sSet?id=1&name=zhouyi&age=35&key=test
    @RequestMapping("/sSet")
    public long sSet(String key, UserEntity user) {
        log.info("sSet key:{},value:{}", key, user);
        return redisUtil.sSet(key, user);
    }

    // http://localhost:8080/sGet?key=test
    @RequestMapping("/sGet")
    public Set<Object> sGet(String key) {
        Set<Object> sGet = redisUtil.sGet(key);
        log.info("sGet key:{},value:{}", key, sGet);
        return sGet;
    }

    // http://localhost:8080/sGetSetSize?key=test
    @RequestMapping("/sGetSetSize")
    public long sGetSetSize(String key) {
        long sGetSetSize = redisUtil.sGetSetSize(key);
        log.info("sGetSetSize key:{},size:{}", key, sGetSetSize);
        return sGetSetSize;
    }

    // ---------------------Set类型---end---------------------------------
    // ---------------------Hash类型---begin---------------------------------
    // http://localhost:8080/hset?id=1&name=zhouyi&age=35&key=test&item=it
    @RequestMapping("/hset")
    public boolean hset(String key, String item, UserEntity user) {
        log.info("hset key:{},item:{},value:{}", key, item, user);
        return redisUtil.hset(key, item, user);
    }

    // http://localhost:8080/hget?key=test&item=it
    @RequestMapping("/hget")
    public Object hget(String key, String item) {
        log.info("hget key:{},item:{}", key, item);
        return redisUtil.hget(key, item);
    }

    // http://localhost:8080/hmset?id=1&name=zhouyi&age=35&key=test
    @RequestMapping("/hmset")
    public boolean hmset(String key, UserEntity user) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", user.getId());
        map.put("name", user.getName());
        map.put("age", user.getAge());
        log.info("hmset key:{},map:{},value:{}", key, map, user);
        return redisUtil.hmset(key, map);
    }

    // http://localhost:8080/hmget?key=test
    @RequestMapping("/hmget")
    public Map<Object, Object> hmget(String key) {
        log.info("hmget key:{}", key);
        return redisUtil.hmget(key);
    }

    // ---------------------Hash类型---end---------------------------------
}
