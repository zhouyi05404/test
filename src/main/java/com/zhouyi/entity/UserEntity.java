package com.zhouyi.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserEntity  implements Serializable{
    private static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private int age;
}
