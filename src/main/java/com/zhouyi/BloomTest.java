package com.zhouyi;

import java.util.ArrayList;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

public class BloomTest {

    private static Integer size = 1000000;

    public static void main(String[] args) {
        BloomFilter<Integer> integerBloomFilter = BloomFilter.create(Funnels.integerFunnel(), size,0.0001);
        for (int i = 0; i < size; i++) {
            // 向我们布隆过滤器中存放100万条数据
            integerBloomFilter.put(i);
        }
    
        ArrayList<Integer> integers = new ArrayList<>();
        for (int j = size; j < size + 100000; j++) {
            if (integerBloomFilter.mightContain(j)) {
                // 将布隆过滤器误判的结果存放到集合中方便后期统计
                integers.add(j);
            }
        }
    
        System.out.println("布隆过滤器误判的结果:" + integers.size());
    
    }
    
    /* public static void main(String[] args) {
        //BloomFilter<Integer> integerBloomFilter = BloomFilter.create(Funnels.integerFunnel(), size, 0.01);
        BloomFilter<Integer> integerBloomFilter = BloomFilter.create(Funnels.integerFunnel(), size);
        for (int i = 0; i < size; i++) {
            // 向我们布隆过滤器中存放100万条数据
            integerBloomFilter.put(i);
        }
        ArrayList<Integer> integers = new ArrayList<>();
        for (int j = size; j < size + 10000; j++) {
            // 使用该pai判断key在布隆过滤器中是否存在 返回true 存在 false  表示不存在
            if (integerBloomFilter.mightContain(j)) {
                // 将布隆过滤器误判的结果存放到集合中方便后期统计
                integers.add(j);
            }
        }
        System.out.println("布隆过滤器误判的结果:" + integers.size());
    }
    */
}
